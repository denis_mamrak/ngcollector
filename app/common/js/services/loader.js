angular.module('collector').service('Loader', function($http) {
	var _this = this;

	_this.busy = false;

	_this.get = function(url){
		_this.busy = true;
		return $http.get(url).success(success).error(error);
	};

	_this.post = function(url, data){
		_this.busy = true;
		return $http.post(url, data).success(success).error(error);
	};

	_this.patch = function(url, data){
		_this.busy = true;
		return $http.patch(url, data).success(success).error(error);
	};

	_this.delete = function(url){
		_this.busy = true;
		return $http.delete(url).success(success).error(error);
	};

	function success(){
		if(!$http.pendingRequests.length){
			_this.busy = false;
		}
	}

	function error(){
		if(!$http.pendingRequests.length){
			_this.busy = false;
		}
	}
});