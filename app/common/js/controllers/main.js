angular.module('collector').controller('Main', function($scope, $http){
	var _this = this;

	_this.menu = {
		active: false,
	}

	_this.menu.toggle = function(){
		_this.menu.active = !_this.menu.active;
	}

});