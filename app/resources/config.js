angular.module('collector').config(function($routeProvider){
	$routeProvider
		.when('/themes/:theme/collections/:collection', {
			templateUrl: '/resources/resources.html',
			controller: 'Resources as resources'
		})
		.when('/collections/:collection', {
			templateUrl: '/resources/resources.html',
			controller: 'Resources as resources'
		})
		.when('/search', {
			templateUrl: '/resources/resources.html',
			controller: 'Resources as resources'
		});
});