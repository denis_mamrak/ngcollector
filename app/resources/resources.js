angular.module('collector').controller('Resources', function(Loader){
	var _this = this;

	Loader
		.get('/api/resources')
		.success(function(response){
			_this.list = response;
		});
});