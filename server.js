var _ = require('lodash');
var url = require('url');
var express = require('express');
var bodyparser = require('body-parser');
var lowrdb = require('./lowrdb.js');
var storage = require('lowdb/lib/file-sync');

var PORT = 8080; 
var server = express();
var schemas = {
	themes: {
		primary: 'id',
		siblings: [
			{
				table: 'collections',
				local: 'collections',
				foreign: 'theme',
				number: 'many'
			}
		]
	},
	collections: {
		primary: 'id',
		siblings: [
			{
				table: 'resources',
				local: 'resources',
				foreign: 'collection',
				number: 'many'
			}
		]
	},
	resources: {
		primary: 'id'
	}
};
var db = lowrdb('db.json', {storage: storage, schemas: schemas});

db.defaults({
	counters: [],
	themes: [],
	collections: [],
	resources: []
}).value();

server.use(express.static('app'));
server.use(bodyparser.json());

server
	.route('/api/themes')
	.get(function(request, response){
		var query = url.parse(request.url, true).query;
		var data = db.collect('themes').filter(query).value() || [];
		respond(response, data);
	})
	.post(function(request, response){
		var id = uid('themes');
		var theme = _.assign({}, request.body, {
			id: id,
		});
		db.get('themes').push(theme).value();
		respond(response, theme);
	});

server
	.route('/api/themes/:id')
	.get(function(request, response){
		var id = _.toNumber(request.params.id);
		var theme = db.collect('themes').find({id: id}).value() || {};
		respond(response, theme);
	});	

server
	.route('/api/collections')
	.get(function(request, response){
		var query = url.parse(request.url, true).query;
		var data = db.collect('collections').filter(query).value() || [];
		respond(response, data);
	})
	.post(function(request, response){
		var id = uid('collections');
		var collection = _.assign({}, request.body, {
			id: id,
		});
		db.get('collections').push(collection).value();
		respond(response, collection);
	});

server
	.route('/api/collections/:id')
	.get(function(request, response){
		var id = _.toNumber(request.params.id);
		var collection = db.collect('collections').find({id: id}).value() || {};
		respond(response, collection);
	});

server
	.route('/api/resources')
	.get(function(request, response){
		var query = url.parse(request.url, true).query;
		var data = db.collect('resources').filter(query).value() || [];
		respond(response, data);
	})
	.post(function(request, response){
		var id = uid('resources');
		var resource = _.assign({}, request.body, {
			id: id,
		});
		db.get('resources').push(resource).value();
		respond(response, resource);
	});

server
	.route('/api/resources/:id')
	.get(function(request, response){
		var id = _.toNumber(request.params.id);
		var resource = db.collect('resources').find({id: id}).value() || {};
		respond(response, resource);
	});

server.listen(PORT, function(){
	console.log('Server is listening on port ' + PORT);
});

function respond(response, data){
	var success = Math.random() >= 0.2;
	var delay = 500 + Math.round(Math.random() * 1000);
	setTimeout(function(){
		response.send(data);
	}, delay);
}

function uid(table){
	var count;
	var counter = db.get('counters').find({table: table}).value();
	if(counter){
		count = counter.count + 1;
		db.get('counters').find({table: table}).assign({count: count}).value();
	}else{
		count = 1;
		db.get('counters').push({table: table, count: count}).value();
	}
	return count;
}