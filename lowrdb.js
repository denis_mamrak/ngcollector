var _ = require('lodash');
var LowDB = require('lowdb');
var lowdb;
var schemas;

var LowRDB = function(name, config){
	lowdb = LowDB(name, config);
	schemas = config.schemas;

	lowdb._.mixin({
		collect: collect,
		purge: purge,
	}, {'chain': true});
	
	return lowdb;
}

function collect(db, table){
	var schema = schemas[table];
	var result = _.map(db[table], function(item){
		return aggregate(item, schema, db);
	});
	return result;
}

function purge(data, table, filter){
	var schema = schemas[table];
	var item = _.find(data, filter);

	_.forEach(schema.children || [], function(relation){
		var criteria = {};
		criteria[relation.foreign] = item[schema.primary];
		lowdb.get(relation.table).remove(criteria).value();
	});

	return _.remove(data, filter);
}

function aggregate(item, schema, db){
	var relations = _.concat(schema.parents || [], schema.siblings || [], schema.children || []);
	var result = {};
	_.forEach(relations, function(relation){
		var data = db[relation.table];
		var criteria = {};
		var related = {};
		criteria[relation.foreign] = item[schema.primary];
		if(relation.number == 'single'){
			related[relation.local] = _.find(data, criteria);
		}
		if(relation.number == 'many'){
			related[relation.local] = _.filter(data, criteria);
		}
		result = _.assign(result, related);
	});
	return _.assign(result, item);
}

module.exports = LowRDB;